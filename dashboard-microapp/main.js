  	$(document).ready(function() {
  		var eventDates = [10];
  		$('.main-calendar').datepicker({
  			language: 'en',
  			inline: true,
  			onRenderCell: function(date, cellType) {
  				var currentDate = date.getDate();
  				if (cellType == 'day' && eventDates.indexOf(currentDate) != -1 && date.getMonth() == 3) {
  					return {
  						html: currentDate + '<span class="dp-note"></span>'
  					}
  				}
  			},
  		});

  		var pieData = {
  			datasets: [{
  				data: [247, 10, 34],
  				backgroundColor: [
	  				'rgba(255, 99, 132)',
	  				'rgba(54, 162, 235)',
	  				'rgba(255, 206, 86)'
	  			]
  			}],
  			labels: [
	  			'Transfer',
	  			'In-App',
	  			'Cash In'
  			]
  		};

  		Chart.defaults.global.hover.mode = 'nearest';
  		var ctx = document.getElementById("myChart").getContext('2d');
  		var myPieChart = new Chart(ctx,{
  			type: 'pie',
  			data: pieData,
  			options: {
  				tooltips: {
  					callbacks: {
  						label: function(tooltipItem, data) {
  							var label = data.labels[tooltipItem.index];
  							var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
  							return label + ': IDR ' + val;
  						}
  					},
  				}
  			},

  		});

  		var dataTrx = {
  			"pubkey":"1K74SQ817UXRwUJB8SAtKqzzPo5tyS1aPT",
  			"start":"20180101",
  			"end":"20180331"
  		};

  		$('.trigger').click(function() {
  			var data = $('.main-calendar').data('datepicker');
  			console.log(data);
  			if (data.selectedDates.length === 0) {
  				console.log('use currentDate');
  				console.log(data.currentDate);
  			} else {
  				console.log('use selectedDates');
  				console.log(data.selectedDates[0]);
  			}

  			$.ajax({
  				url: 'http://37cba8ce.ngrok.io/apiv1/sumTrx',
  				type: 'POST',
  				data: JSON.stringify(dataTrx),
  				success: function(res) {
  					console.log(res);
  				}
  			});
  		});

  		function addData(chart, data, datasetIndex) {
  			chart.data.datasets[datasetIndex].data = data;
  			chart.update();
  		}

  		$('.trigger2').click(function() {
  			addData(myPieChart, [5, 9, 12], 0);
  		});

      $('.more-btn').click(function() {
        $('.more-container').css('transform', 'translate(0)');
      });

      $('.back-arrow').click(function() {
        $('.more-container').css('transform', 'translate(110%)');
      });

      $('.tab-transfer').click(function() {
        $('.more-container .tab').not(this).removeClass('active');
        $(this).addClass('active');
        $('.table-transfer').show();
        $('.table-inapp, .table-cashin').hide();
      });

      $('.tab-inapp').click(function() {
        $('.more-container .tab').not(this).removeClass('active');
        $(this).addClass('active');
        $('.table-inapp').show();
        $('.table-transfer, .table-cashin').hide();
      });

      $('.tab-cashin').click(function() {
        $('.more-container .tab').not(this).removeClass('active');
        $(this).addClass('active');
        $('.table-cashin').show();
        $('.table-inapp, .table-transfer').hide();
      });

  	});