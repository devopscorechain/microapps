import MainPage from './components/MainPage.vue';
import PulsaPage from './components/PulsaPage.vue';
import PaketDataPage from './components/PaketDataPage.vue';
import TokenListrikPage from './components/TokenListrikPage.vue';

export default [
	{path: '/', component: MainPage},
	{path: '/pulsa', component: PulsaPage},
	{path: '/paket-data', component: PaketDataPage},
	{path: '/token-listrik', component: TokenListrikPage}
]