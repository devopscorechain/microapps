$(document).ready(function() {
	var myId;
	var friendId;
	var listMessage; // Global Variable
	var currentChannel; // Global Variable
	var currentPhoto; // Global Variable
	var currentChatWith; //Gloval Variable

	lottie.loadAnimation({
  	container: document.getElementById('animation'), // the dom element that will contain the animation
  	renderer: 'svg',
  	loop: true,
  	autoplay: true,
  	path: 'assets/js/mail-animation.json' // the path to the animation json
	});

	var sb = new SendBird({
		appId: '7F2318B6-12E1-49DA-B8B3-5EA52D60463C'
	});

	function scrollBottom() {
		var objDiv = document.getElementById("message-content");
		objDiv.scrollTop = objDiv.scrollHeight;
	}

	var ChannelHandler = new sb.ChannelHandler();
	ChannelHandler.onMessageReceived = function(channel, message){
		console.log('dapet pesan');
		console.log(message);
		var sender = message._sender.userId;
		var receiveMsgType = message.messageType;
		if (receiveMsgType == 'file') {
			var newMessage = message.url;
		} else {
			var newMessage = message.message;
		}
		updateLastMessage(sender, newMessage);
		updateChatMessage(sender, newMessage, receiveMsgType);
		if ($('.list-message').filter('[data-chatwith="'+message._sender.userId+'"]').data('chatwith') == undefined) {
			newListUser(message._sender.userId, message._sender.profileUrl, message.message);
		}
	};
	sb.addChannelHandler('channel', ChannelHandler);

	var ConnectionHandler = new sb.ConnectionHandler();

ConnectionHandler.onReconnectStarted = function(){
	console.log('Reconnecting..');
};
ConnectionHandler.onReconnectSucceeded = function(){
	console.log('Reconnect Succeeded');
	initApp();
	getListMessage();
};
ConnectionHandler.onReconnectFailed = function(){};

sb.addConnectionHandler('connection', ConnectionHandler);

	function connect(id) {
		sb.connect(id, function(user, error) {
			if (error) console.log(error);
			console.log('success connect');
			console.log(user);
			initApp(); // Harus di jalankan agar bisa memanggil variable listMessage
			getListMessage(); // Optional, dijalankan langsung setelah connect
			$('.welcome-screen').remove();
			$('.list-user').css('display', 'flex');
			$('.list-message-loading').css('display', 'block');
		});
	}

	function initApp() {
		listMessage = sb.GroupChannel.createMyGroupChannelListQuery();
		listMessage.includeEmpty = false;
		listMessage.limit = 20;
	}

	function getListMessage() {
		listMessage.next(function(channels, error) {
			if (error) console.log(error);
			console.log(channels);
			$('.list-message-container').empty();
			for (var x = 0; x < channels.length; x++) {
				if (channels[x].members.length !== 1) {
					for (var y = 0; y < channels[x].members.length; y++) {
						if (channels[x].members[y].userId !== myId) {
							var friendName = channels[x].members[y].userId;
							var friendPhoto = channels[x].members[y].profileUrl;
							if (friendPhoto == '') {
								friendPhoto = 'https://sendbird.com/main/img/profiles/profile_01_512px.png';
							}
						}
					}
				} else {
					var friendName = channels[x].members[0].userId;
					var friendPhoto = channels[x].members[0].profileUrl;
					if (friendPhoto == '') {
						friendPhoto = 'https://sendbird.com/main/img/profiles/profile_01_512px.png';
					}
				}

				if (channels[x].unreadMessageCount !== 0) {
					var unread = '<p class="unread">' + channels[x].unreadMessageCount + '</p>';
				} else if (channels[x].unreadMessageCount === 0) {
					var unread = '';
				}

				if (channels[x].lastMessage.messageType == 'file') {
					var lastMessageText = '(file message)';
				} else {
					var lastMessageText = channels[x].lastMessage.message;
				}

				$('.list-message-container').append(`
					<div class="list-message" data-chatwith="` + friendName + `">
						<span class="avatar-big"><img class="avatar-img" src='` + friendPhoto + `'></span>
						<div class="user-info">
							<div class="user-name-notif"><h1>` + friendName + `</h1>` + unread + `</div>
							<p class="last-message">`+ lastMessageText +`</p>
						</div>
					</div>
				`);
			}
		});
	}

	function updateLastMessage(senderId, senderMsg) {
		$(".list-message[data-chatwith='" + senderId +"']").prependTo('.list-message-container')
		$(".list-message[data-chatwith='" + senderId +"'] .last-message").text(senderMsg);
		console.log($(".list-message[data-chatwith='" + senderId +"'] .unread").text())
		if ($(".list-message[data-chatwith='" + senderId +"'] .unread").text() === '') {
			$(".list-message[data-chatwith='" + senderId +"'] .user-name-notif").append('<p class="unread">1</p>');
		} else {
			var unread = parseInt($("div[data-chatwith='" + senderId +"'] .unread").text());
			$(".list-message[data-chatwith='" + senderId +"'] .unread").text(Number(unread + 1));
		}
	}

	function enterChatRoom(frId) {
		currentChatWith = frId;
		sb.GroupChannel.createChannelWithUserIds([frId], true, function(channel, error) {
			if (error) console.log(error);
			currentChannel = channel;
			loadMessage();
		});
	}

	function loadMessage() {
		var messageListQuery = currentChannel.createPreviousMessageListQuery();
		messageListQuery.load(30, true, function(messageList, error) {
			console.log(messageList);
			$('.friend-name').text(currentChatWith);
			$('.avatar-small').append('<img class="avatar-img" src="'+ currentPhoto +'">');
			for (var x = messageList.length - 1; x >= 0; x--) {
				if (messageList[x].messageType == 'file') {
					var contentMessage = '<img class="message-image" src="'+messageList[x].url+'">';
				} else {
					var contentMessage = '<p class="message-text">'+messageList[x].message+'</p>';
				}
				if (messageList[x]._sender.userId == myId) {
					$('.message-content').append(`
						<div class="message-box from-me">
							<div class="message-chat from-me">
								`+contentMessage+`
							</div>
						</div>
						`);
				} else {
					$('.message-content').append(`
						<div class="message-box from-friend">
							<div class="message-chat from-friend">
							`+contentMessage+`
							</div>
						</div>
					`);
				}
			}
			scrollBottom();
			currentChannel.markAsRead();
			console.log('current chat with ' + currentChatWith);
		});
	}

	function updateChatMessage(senderId, updateMsg, msgType) {
		if (senderId == currentChatWith && $('.message-container').css('display') == 'flex') {
			if (msgType == 'file' ) {
				$('.message-content').append(`
					<div class="message-box from-friend">
						<div class="message-chat from-friend">
							<img class="message-image" src="`+updateMsg+`">
						</div>
					</div>
				`);
			} else {
				$('.message-content').append(`
					<div class="message-box from-friend">
						<div class="message-chat from-friend">
							<p class="message-text">`+updateMsg+`</p>
						</div>
					</div>
				`);
			}
			scrollBottom();
		}
	}

	function newListUser(newFriendUser, newFriendPhoto, newLastMessage) {
		$('.list-message-container').prepend(`
			<div class="list-message" data-chatwith="` + newFriendUser + `">
				<span class="avatar-big"><img class="avatar-img" src='` + newFriendPhoto + `'></span>
				<div class="user-info">
					<div class="user-name-notif"><h1>` + newFriendUser + `</h1><p class="unread">1</p></div>
					<p class="last-message">`+ newLastMessage +`</p>
				</div>
			</div>
		`);
	}

	function sendMessage(msg) {
		currentChannel.sendUserMessage(msg, function(message, error){
			if (error) console.error(error);
			console.log(message);
			$('.message-content').append(`
				<div class="message-box from-me">
					<div class="message-chat from-me">
						<p>`+message.message+`</p>
					</div>
				</div>
			`);
			scrollBottom();
			updateLastMessage(currentChatWith, msg);
		});
	}

	function sendFile(file) {
		currentChannel.sendFileMessage(file, function(fileMessage, error){
			if (error) console.error(error);
			$('.message-content').append(`
				<div class="message-box from-me">
					<div class="message-chat from-me">
						<img class="message-image" src="` + fileMessage.url + `">
					</div>
				</div>
			`);
			scrollBottom();
			$('.send-file-loading').css('display', 'none');
			$('.message-input').attr('contenteditable', true);
			updateLastMessage(currentChatWith, '(file message)');
		});
	}

	function newMessageChat(newMsgId) {
		sb.GroupChannel.createChannelWithUserIds([newMsgId], true, function(channel, error) {
			if (error) {
				console.log(error);
				$('.warning-error').css('display', 'block');
				return;
			}
			console.log(channel);
			currentChannel = channel;
			$('#input-newmsg').val('');
			$('.username-list').empty();
			$('.modal-new-message').css('display', 'none');
			$('.list-user').css('display', 'none');
			$('.message-container').css('display', 'flex');
			$('.friend-name').text(newMsgId);
			if (channel.members.length > 1) {
				for (var x = 0; x < channel.members.length; x++) {
					if (channel.members[x].userId !== myId) {
						currentPhoto = channel.members[x].profileUrl;
					}
				}
			} else {
				currentPhoto = channel.members[0].profileUrl;
			}
			$('.avatar-small').append('<img class="avatar-img" src="'+ currentPhoto +'">');
			if (channel.lastMessage !== null) {
				loadMessage();
			}
		});
	}

	function fixNotif() {
		currentChannel.markAsRead();
		$(".list-message[data-chatwith='" + currentChatWith +"']").find('.unread').remove();
	}

	$('#btn-login').click(function() {
		myId = $('#id-login').val();
		if (myId.length > 0) {
			connect(myId);
			$('.login-loading').css('display', 'block');
			$(this).attr('disabled', true);
		}
	});

	$('.message-button').click(function() {
		if (!$(this).hasClass('btn-active')) {
			$(this).addClass('btn-active');
			$('.modal-new-message').fadeIn(300);
		} else {
			$(this).removeClass('btn-active');
			$('.modal-new-message').fadeOut(300);
		}
	});

	$(document).on('click', '.list-message', function() {
		$('.list-user').css('display', 'none');
		$('.message-container').css('display', 'flex');
		friendId = $(this).data('chatwith');
		currentPhoto = $(this).find('.avatar-img').attr('src');
		enterChatRoom(friendId);
		$(this).find('.unread').remove();
	});

	$('.back-button').click(function() {
		$('.message-container').css('display', 'none');
		$('.list-user').css('display', 'flex');

		$('.message-button').removeClass('btn-active');
		$('.friend-name').text('');
		$('.avatar-small').empty();
		$('.message-content').empty();
		fixNotif();
	});

	$('.send-button').click(function() {
		var textMessage = $('.message-input').text();
		sendMessage(textMessage);
		$('.message-input').text('');
	});

	$('#input-file').change(function() {
		$('.message-input').attr('contenteditable', false);
		$('.send-file-loading').css('display', 'block');
		var file = $('#input-file')[0].files[0];
		sendFile(file);
	});


	$('.message-input').keydown(function(event) {
		var x = event.which || event.keyCode;
		if (event.keyCode == 13) {
			if ($('.message-input').text().length == 0) {
				return false;
			} else {
				var textMessage = $('.message-input').text();
				sendMessage(textMessage);
				$('.message-input').text('');
				return false;
			}
		}
	});

	$('#btn-newmsg').click(function() {
		var newMessageId = $('#input-newmsg').val();
		if (newMessageId !== myId ) {
			newMessageChat(newMessageId);
		} else {
			$('.warning-error').css('display', 'block');
		}
	});

	// $('#input-newmsg').on('input', function() {
	// 	var inputLength = $('#input-newmsg').val().length;
	// 	if (inputLength === 0) {
	// 		$('.username-list').empty();
	// 	} else {
	// 		var search = $('#input-newmsg').val();
	// 		var count = 0;
	// 		$.getJSON("assets/js/username.json", function(result){
	// 			var output = '';
	// 			$.each(result, function(key, value) {
	// 				if (value.username.search(search) != -1) {
	// 					if (count < 5) {
	// 						output += '<li class="username-name">' + value.username + '</li>';
	// 					}
	// 					count++;
	// 				}
	// 			});
	// 			$('.username-list').html(output);
	// 		});
	// 	}
	// });

	// $(document).on('click', '.username-name', function() {
	// 	var clickText = $(this).text();
	// 	$('#input-newmsg').val(clickText);
	// 	$('.username-list').empty();
	// });

	$(document).on('click', '.message-image', function() {
		var imageSrc = $(this).attr('src');
		$('.image-modal-content').attr('src', imageSrc);
		$('.image-modal-container').css('display', 'flex');
	});

	$('.close-modal-btn').click(function() {
		$('.image-modal-container').css('display', 'none');
		$('.image-modal-content').attr('src', '');
	});

	$('.button-coin-money').click(function() {
		$('.modal-money-container').css('display', 'flex');
	});

	$('.button-money.btn-cancel').click(function() {
		$('.modal-money-container').css('display', 'none');
		$('.input-money').val('');
	});
});